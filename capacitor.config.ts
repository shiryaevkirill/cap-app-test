import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.example.app',
  appName: 'cap-app-test',
  webDir: 'build',
  bundledWebRuntime: false,
  server: {
    url: "http://192.168.1.103:3000"
  },
};

export default config;
