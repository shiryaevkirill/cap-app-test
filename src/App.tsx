import React, {Fragment, useState} from 'react';
import {Space, Button, Typography, Row, Col, Input, List} from 'antd';
import {Camera, CameraSource, CameraResultType} from '@capacitor/camera';
import {Filesystem, Directory, Encoding} from '@capacitor/filesystem';
const {Title, Text} = Typography;

function App() {
  const [fileText, setFileText] = useState('');
  const [fileData, setFileData] = useState<string[]>([]);
  const titles = ['Дата создания: ', 'Дата изменения: ', 'Размер: ', 'Текст: '];

  const handleGetPhoto = async (source: CameraSource) => {
    const image = await Camera.getPhoto({
      source: source,
      resultType: CameraResultType.Uri,
    });
  };

  const handleWriteSecretFile = async () => {
    await Filesystem.writeFile({
      path: 'file.txt',
      data: fileText,
      directory: Directory.Cache,
      encoding: Encoding.UTF8,
    });
    setFileData([]);
  };

  const handleReadSecretFile = async () => {
    try {
      const contents = await Filesystem.readFile({
        path: 'file.txt',
        directory: Directory.Cache,
        encoding: Encoding.UTF8,
      });

      const stats = await Filesystem.stat({
        path: 'file.txt',
        directory: Directory.Cache,
      });

      const ctime = new Date(stats.ctime ? stats.ctime : 0).toLocaleString();
      const mtime = new Date(stats.mtime ? stats.mtime : 0).toLocaleString();
      const data = [ctime, mtime, stats.size.toString(), contents.data];
      setFileData(data);
    } catch {
      console.log('test error');
      setFileData(['Ошибка: Файл не создан!']);
    }
  };

  return (
    <Fragment>
      <Row>
        <Col span={24} className="text-center">
          <Space direction="vertical">
            <Title level={5}>Camera</Title>
            <Button
              type="primary"
              onClick={() => handleGetPhoto(CameraSource.Camera)}
            >
              Открыть камеру
            </Button>
            <Button
              type="primary"
              onClick={() => handleGetPhoto(CameraSource.Photos)}
            >
              Открыть галерею
            </Button>
          </Space>
        </Col>
      </Row>
      <Row>
        <Col span={24} className="text-center">
          <Space direction="vertical">
            <Title level={5}>FileSystem</Title>
            <Input
              placeholder="Введите текст файла"
              onChange={(e) => setFileText(e.currentTarget.value)}
            />
            <Button
              type="primary"
              disabled={fileText === '' ? true : false}
              onClick={() => handleWriteSecretFile()}
            >
              Создать файл
            </Button>
            <Button type="primary" onClick={() => handleReadSecretFile()}>
              Информация о файле
            </Button>
          </Space>
        </Col>
      </Row>
      <List
        className="ml-5"
        dataSource={fileData}
        renderItem={(item, number) =>
          fileData.length > 1 ? (
            <List.Item>
              <Text>{titles[number] + item}</Text>
            </List.Item>
          ) : (
            <List.Item>
              <Text>{item}</Text>
            </List.Item>
          )
        }
        locale={{emptyText: ' '}}
      />
    </Fragment>
  );
}

export default App;
